//apigee integrated
//opt-out
export const OPT_OUT_URL = "https://dev-dot-user-group-management-dot-pid-gousenapb-noti-res02.appspot.com/api/users/opt-out/"

//unsubscription from group
export const UNSUBSCRIBE_FROM_GROUP_URL = "https://dev-dot-user-group-management-dot-pid-gousenapb-noti-res02.appspot.com/api/users/unsubscribe/"

//manage subscriptions
export const GET_APPLICATION_SEGREGATED_ASSOCIATION_OF_USER_GROUP_MAPPING = "https://dev-dot-user-group-management-dot-pid-gousenapb-noti-res02.appspot.com/api/users/manage-subscriptions/"
export const URL_FOR_REMOVING_USER_GROUP_ASSOCIATION_IN_BULK = "https://dev-dot-user-group-management-dot-pid-gousenapb-noti-res02.appspot.com/api/users/manage-subscriptions/"

//application unsubscription
export const APPLICATION_UNSUBSCRIPTION_BACKEND_URL = "https://dev-dot-user-group-management-dot-pid-gousenapb-noti-res02.appspot.com/api/users/unsubscribe/application/"





//apigee integrated
//opt-out
export const OPT_OUT_URL_APIGEE = "https://apis-dev.globalpay.com/gnf/users/opt-out/"

//unsubscription from group
export const UNSUBSCRIBE_FROM_GROUP_URL_APIGEE = " https://apis-dev.globalpay.com/gnf/users/unsubscribe/"

//manage subscriptions
export const GET_APPLICATION_SEGREGATED_ASSOCIATION_OF_USER_GROUP_MAPPING_APIGEE = "https://apis-dev.globalpay.com/gnf/users/manage-subscriptions/"
export const URL_FOR_REMOVING_USER_GROUP_ASSOCIATION_IN_BULK_APIGEE = "https://apis-dev.globalpay.com/gnf/users/manage-subscriptions/"

//application unsubscription
export const APPLICATION_UNSUBSCRIPTION_BACKEND_URL_APIGEE = "https://apis-dev.globalpay.com/gnf/users/unsubscribe/application/"