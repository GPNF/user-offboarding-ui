export const HEADER_EMAIL_VALUE = 'asinha161@gmail.com'
export const HEAD_EMAIL_KEY = 'user_email'

//services
export const UNSUBSCRIBE = "unsubscribe"
export const APPLICATION_UNSUBSCRIBE = "appunsubscribe"
export const OPT_OUT = "optout"
export const MANAGE_SUBSCRIPTIONS = "manageSubscriptions"

//token constants
export const EQUALS = "="
export const AMPERSAND = "&"